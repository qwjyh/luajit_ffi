local ffi = require("ffi")
local customlib = ffi.load("shared_lib")

ffi.cdef [[
  int return1(int i);

  typedef unsigned long DWORD;
  DWORD GetCurrentProcessId(void);
]]

print(customlib.return1(1))

local pid = ffi.C.GetCurrentProcessId()
print("pid : " .. pid)